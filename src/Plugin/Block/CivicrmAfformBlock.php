<?php

namespace Drupal\civicrm_afform_block\Plugin\Block;

use Civi\Api4\Afform;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'CiviCRM Afform' Block.
 *
 * @Block(
 *   id = "civicrm_afform_block",
 *   admin_label = @Translation("CiviCRM Afform Block"),
 *   category = @Translation("Civicrm"),
 * )
 */
class CivicrmAfformBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    return [
      '#theme' => 'civicrm_afform_block',
      '#name' => $config['civicrm_afform_block_form_name'],
      '#directive' => $config['civicrm_afform_block_directive_name'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    \Drupal::service('civicrm')
      ->initialize();
    $afforms = Afform::get()
      ->addSelect('module_name', 'title')
      ->addWhere('type', 'IN', ['form', 'search', 'system'])
      ->execute();
    foreach ($afforms as $afform) {
      if ($afform['title']) {
        $optionsArray[$afform['module_name']] = $afform['title'];
      }
      else {
        $optionsArray[$afform['module_name']] = $afform['module_name'];
      }
    }
    $form['civicrm_afform_block_form_name'] = [
      '#type' => 'select',
      '#title' => $this->t('CiviCRM Form Name'),
      '#description' => $this->t('Which form would you like to display?'),
      '#default_value' => $config['civicrm_afform_block_form_name'] ?? '',
      '#options' => $optionsArray,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $getDirectiveName = Afform::get(FALSE)
      ->addWhere('name', '=', $values['civicrm_afform_block_form_name'])
      ->setLimit(1)
      ->addSelect('directive_name')
      ->execute();
    $this->configuration['civicrm_afform_block_directive_name'] = $getDirectiveName[0]['directive_name'];
    $this->configuration['civicrm_afform_block_form_name'] = $values['civicrm_afform_block_form_name'];
  }

}
