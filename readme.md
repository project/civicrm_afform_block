# CiviCRM Afform Block

## Description

A lightweight module allowing CiviCRM native forms from the Form
Builder extension to be placed on the front-end of your Drupal
site using blocks.

## Requirements

This module requires CiviCRM installed, as well as the form core
extension for CiviCRM. The Form Builder extension for CiviCRM
is also recommended to see and configure forms via GUI.

## Use

0) Meet installation requirements
1) Install this module as normal
2) Place a new block of type "CiviCRM Afform Block" and choose
the name of the form you would like to see from the drop-down

## Troubleshooting and Bugs

TBD
